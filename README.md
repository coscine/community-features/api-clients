# Coscine API Clients
This repository includes a CI Pipeline responsible for automatically building API Clients for various programming languages.  
The build files are accessible under `Assets/other` in the [repository's releases](https://git.rwth-aachen.de/coscine/community-features/api-clients/-/releases).

## Supported Programming languages
- python
- javascript
- dart
- csharp
- go
- java
- kotlin
- R
- scala
- swift5
